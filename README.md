# Template

Esse é um template para utilização em aplicações que utilizem:

* Python
* Flask
* SQLAlchemy
* Psycopg2
* Migrations



### Para iniciar o repositório rode os seguintes comandos no terminal:

`python -m venv venv --upgrade-deps`

`source venv/bin/activate`

`pip install -r requirements.txt`

### Para iniciar as migrations use os seguites comandos em sequência:

`flask db init`

`flask db migrate -m 'Iniciando repositório'`

#### Para atualizar seu banco utilize
`flask db upgrade`
