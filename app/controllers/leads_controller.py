from datetime import datetime
from flask import jsonify, request, current_app
from app.models.leads_model import Leads
import re
import sqlalchemy
import pdb

def create_one():
    data = request.get_json()
    phone_regex = '^\([1-9]{2}\)[0-9]{5}\-[0-9]{4}$'
    re_match = re.fullmatch(phone_regex, data['phone'])
    data_keys = data.keys()
    try:
        if re_match != None:
            if 'phone' and 'email' and 'name' in data_keys:
                if data['name'] or data['email'] or data['phone'] == str:
                    lead = Leads(**data)

                    current_app.db.session.add(lead)
                    current_app.db.session.commit()

                    return jsonify(lead)
                else:
                    return {"message": "os dados devem estar no formato de string"}, 400
            else:
                return {"message": "a requisicao deve obrigat conter name, email e phone e deve ser uma string"}, 400

        else:
            return {"message": "padrao de telefone incorreto"}, 400
    except sqlalchemy.exc.IntegrityError:
        return {"message": "name, email ou phone ja cadastrados"}, 400


def get_all():
    leads = Leads.query.all()
    return jsonify(leads)


def update_one():
    try:
        data = request.get_json()

        lead = Leads.query.filter_by(email = data['email']).first()
        lead.visits = lead.visits + 1
        lead.last_visit = datetime.utcnow()

        current_app.db.session.add(lead)
        current_app.db.session.commit()

        return jsonify(lead)
    except BaseException:
        return {"message": "o corpo deve conter um json valido"}, 400


def delete_one():
    data = request.get_json()
    data_keys = data.keys()
    try:
        if len(data_keys) == 1 and 'email' in data_keys and data['email'] == str:
            lead = Leads.query.filter_by(email = data['email']).first()
    
            current_app.db.session.delete(lead)
            current_app.db.session.commit()
    
            return "", 204
        else:
            return {"message":"deve ser passado obrigatoriamente um email em formato de string no corpo da requisicao"}
    except sqlalchemy.orm.exc.UnmappedInstanceError:
        return {"message": "email nao encontrado"}, 400
