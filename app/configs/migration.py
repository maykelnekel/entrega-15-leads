from flask import Flask
from flask_migrate import Migrate

def init_app(app: Flask):
    
    # IMPORTE AQUI SUAS CLASSES
    from app.models.leads_model import Leads

    Migrate(app, app.db)
