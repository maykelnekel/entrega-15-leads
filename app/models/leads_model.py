from dataclasses import dataclass

from app.configs.database import db
from sqlalchemy import Integer, String, DateTime, Column
from datetime import datetime

@dataclass
class Leads(db.Model):
    id: int
    name: str
    email: str
    phone: str
    creation_date: str
    last_visit: str
    visits: int

    __tablename__ = "leads"

    id = Column(Integer, primary_key=True ,nullable=True)
    name = Column(String, nullable=False, unique=True)
    email = Column(String, nullable=False, unique=True)
    phone = Column(String, nullable=False, unique=True)
    creation_date = Column(DateTime, nullable=True, default=datetime.utcnow)
    last_visit = Column(DateTime, nullable=True, default=datetime.utcnow)
    visits = Column(Integer, nullable=True, default=1)


