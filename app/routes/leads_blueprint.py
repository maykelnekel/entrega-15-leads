from flask import Blueprint
from app.controllers.leads_controller import create_one, get_all, delete_one, update_one

bp = Blueprint('ex_bp', __name__, url_prefix='/leads')


bp.post("")(create_one)
bp.get("")(get_all)
bp.patch("")(update_one)
bp.delete("")(delete_one)

